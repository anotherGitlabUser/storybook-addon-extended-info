import React from "react";
import marked from "marked";
import { addons, types } from "@storybook/addons";
import { AddonPanel } from "@storybook/components";
import { useParameter } from "@storybook/api";
import { useStorybookApi, useStorybookState } from "@storybook/api";

// for dev
// const context = require.context('!raw-loader!../../../stories', true, /\.md$/);
const context = require.context("!raw-loader!../../../../../src", true, /\.md$/);

const mdFiles = {};

for (let filename of context.keys()) {
  mdFiles[filename] = context(filename);
}


const ADDON_ID = "extendedInfo";
const PANEL_ID = `${ADDON_ID}/panel`;
const PARAM_KEY = "extendedInfo";

const getReadmeFromStoryId = ({ mdFile = "" }) => {
  try {
    const result = Object.keys(mdFiles)
      .filter((mdFilePath) => mdFilePath.toLowerCase().includes(mdFile.toLowerCase()));

    // todo: we could have multiple results
    if (result.length > 0) return marked(mdFiles[result[0]].default);
    return "No matching Markdown file found";
  } catch (error) {
    console.error("Something went wrong loading .md files", error);
    return "";
  }
};

// give a unique name for the panel
const MyPanel = () => {
  const state = useStorybookState();

  const value = useParameter(PARAM_KEY, null);
  const mdFile = value?.data ? value.data : state.storyId;

  const htmlSource = getReadmeFromStoryId({ mdFile });
  return <div dangerouslySetInnerHTML={{ __html: htmlSource }} />;
};


addons.register(ADDON_ID, (api) => {
  addons.add(PANEL_ID, {
    type: types.PANEL,
    title: "Extended Info",
    render: ({ active, key }) => {
      return (
        <AddonPanel active={active} key={key}>
          <MyPanel />
        </AddonPanel>
      );
    }
  });
});
